package AIF.AerialVehicles.FighterJets;

import AIF.AerialVehicles.AerialAttackVehicle;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.AttackMission;
import AIF.Missions.Mission;

public abstract class FighterJet extends AerialVehicle implements AerialAttackVehicle {
    private static final int MAX_HOURS_AFTER_REPAIR = 250;
    int amountOfMissile;
    String missileModel;

    public FighterJet(int amountOfMissile, String missileModel, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.amountOfMissile = amountOfMissile;
        this.missileModel = missileModel;
    }

    @Override
    public String attack() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " Attacking " + ((AttackMission) this.getMission()).getTarget() +
                " with: " + this.getMissileModel() + "X" + this.getAmountOfMissile();
        System.out.println(message);
        return message;
    }

    @Override
    public void check() {
        if (this.getHoursOfFlightSinceLastRepair() >= MAX_HOURS_AFTER_REPAIR){
            this.repair();
        }
    }
//    @Override
//    public void check() {
//        if(this.getHoursOfFlightSinceLastRepair() < 0)
//            throw new IllegalArgumentException("hoursOfFlightSinceLastRepair must be positive");
//        if (this.getHoursOfFlightSinceLastRepair() >= MAX_HOURS_AFTER_REPAIR){
//            this.repair();
//        }
//    }

    public int getAmountOfMissile() {
        return this.amountOfMissile;
    }

    public String getMissileModel() {
        return this.missileModel;
    }
}
