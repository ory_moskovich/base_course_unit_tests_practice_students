package AIF;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.FighterJets.F15;
import AIF.AerialVehicles.FighterJets.F16;
import AIF.AerialVehicles.UAVs.Haron.Eitan;
import AIF.AerialVehicles.UAVs.Haron.Shoval;
import AIF.AerialVehicles.UAVs.Hermes.Kochav;
import AIF.AerialVehicles.UAVs.Hermes.Zik;
import AIF.Entities.Coordinates;
import AIF.Missions.AttackMission;
import AIF.Missions.BdaMission;
import AIF.Missions.IntelligenceMission;
import AIF.Missions.Mission;

import java.util.HashMap;


public class AIFUtil {


    private   HashMap<String, Mission> allMissions ;//= getAllMisstion();
    private   HashMap<String,AerialVehicle> aerialVehiclesHashMap ;//= getAllAerialVehicles();

    /**
     * Initiate object in order to use class util.
     */
    public AIFUtil() {
        allMissions = getAllMisstion();
        aerialVehiclesHashMap = getAllAerialVehicles();
    }

    public  void setHoursAndCheck(AerialVehicle vehicle, int hours){
        vehicle.setHoursOfFlightSinceLastRepair(hours);
        vehicle.check();
    }

    public HashMap<String, Mission> getAllMissions() {
        return allMissions;
    }

    public HashMap<String, AerialVehicle> getAerialVehiclesHashMap() {
        return aerialVehiclesHashMap;
    }

    private   HashMap<String, Mission> getAllMisstion(){
        HashMap<String, Mission> allMissionHashMap = new HashMap<String, Mission>();
        //create coordinates and missions.
        Coordinates coordinatesToAttack = new Coordinates(31.389906, 34.330190);
        allMissionHashMap.put("bda",new BdaMission("suspect house", coordinatesToAttack));
        allMissionHashMap.put("attack",new AttackMission("suspect house", coordinatesToAttack));
        allMissionHashMap.put("intelligence",new IntelligenceMission("Deir al Balah", coordinatesToAttack));
        return allMissionHashMap;
    }

    private   HashMap<String, AerialVehicle> getAllAerialVehicles(){
        //Create aerial vehicles HashMap.
        HashMap<String,AerialVehicle> aerialVehiclesHashMap = new HashMap<String,AerialVehicle>();// Etan , Shoval , Kohav , Zik , F15 , F16 .
        aerialVehiclesHashMap.put("F15",new F15("elint", 2, "Spice250", "Donald Duck", allMissions.get("attack"), 10, true));
        aerialVehiclesHashMap.put("F16", new F16("thermal", 2, "Spice250", "Tuli", allMissions.get("attack"), 5, true));
        aerialVehiclesHashMap.put("Eitan",new Eitan(1, "nimrod", "elint", "Sheleg", allMissions.get("intelligence"), 52, true));
        aerialVehiclesHashMap.put("Shoval", new Shoval("B&W", 2, "Spice250", "commint", "Lagertha", allMissions.get("bda"), 41, true));
        aerialVehiclesHashMap.put("Kochav",new Kochav(3, "Dlila", "thermal", "commint", "Snoop", allMissions.get("attack"), 31, true));
        aerialVehiclesHashMap.put("Zik",new Zik("thermal", "elint", "Fireman Sam", allMissions.get("bda"), 14, true));
        return aerialVehiclesHashMap;
    }
}
