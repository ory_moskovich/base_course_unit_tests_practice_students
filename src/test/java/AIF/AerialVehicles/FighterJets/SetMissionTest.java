package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.UAVs.Haron.Eitan;
import AIF.AerialVehicles.UAVs.Haron.Shoval;
import AIF.AerialVehicles.UAVs.Hermes.Kochav;
import AIF.App;
import AIF.Entities.Coordinates;
import AIF.Missions.*;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertNotNull;

public class SetMissionTest extends TestCase {

    HashMap<String, AerialVehicle> avs;
    Mission attackMission;
    Mission bdaMission;
    Mission intelligenceMission;


    @Before
    public void initiate(){
        AIFUtil aifUtil = new AIFUtil();
        this.avs = aifUtil.getAerialVehiclesHashMap();
        attackMission = aifUtil.getAllMissions().get("attack");
        bdaMission = aifUtil.getAllMissions().get("bda");
        intelligenceMission = aifUtil.getAllMissions().get("intelligence");

    }

    //F15
    @Test(expected = MissionTypeException.class)
    public void F15AttackMission() throws MissionTypeException {
        avs.get("F15").setMission(attackMission);
    }

    @Test(expected = MissionTypeException.class)
    public void F15IntelligenceMission() throws MissionTypeException {
        avs.get("F15").setMission(intelligenceMission);
    }

    @Test
    public void F15BDAMission() throws MissionTypeException {
        avs.get("F15").setMission(bdaMission);
    }

    //F16
    @Test(expected = MissionTypeException.class)
    public void F16AttackMission() throws MissionTypeException {
        avs.get("F16").setMission(attackMission);
    }

    @Test
    public void F16IntelligenceMission() throws MissionTypeException {
        avs.get("F16").setMission(intelligenceMission);
    }

    @Test(expected = MissionTypeException.class)
    public void F16BDAMission() throws MissionTypeException {
        avs.get("F16").setMission(bdaMission);
    }

    //Eitan
    @Test(expected = MissionTypeException.class)
    public void EitanAttackMission() throws MissionTypeException {
        avs.get("Eitan").setMission(attackMission);
    }

    @Test(expected = MissionTypeException.class)
    public void EitanIntelligenceMission() throws MissionTypeException {
        avs.get("Eitan").setMission(intelligenceMission);
    }

    @Test
    public void EitanBDAMission() throws MissionTypeException {
        avs.get("Eitan").setMission(bdaMission);
    }

    //Zik
    @Test
    public void ZikAttackMission() throws MissionTypeException {
        avs.get("Zik").setMission(attackMission);
    }

    @Test(expected = MissionTypeException.class)
    public void ZiknIntelligenceMission() throws MissionTypeException {
        avs.get("Zik").setMission(intelligenceMission);
    }

    @Test(expected = MissionTypeException.class)
    public void ZikBDAMission() throws MissionTypeException {
        avs.get("Zik").setMission(bdaMission);
    }

}